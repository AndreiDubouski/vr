﻿using UnityEngine;

public class DestroyGameObjectTwo : MonoBehaviour {
	
	public GameObject ObjectNew;
	public GameObject ObjectOld;
	public static int HealthTwo = 1;

    private Vector3 ballNumber4PartsStartPos = new Vector3(17.04f, 35.27f, 30.27f);

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (HealthTwo <= 0) {
			//ObjectNew.SetActive (true);
			ObjectOld.SetActive (false);

            GameObject clone = Instantiate(ObjectNew, Vector3.one, Quaternion.identity) as GameObject;
            // Modify the clone to your heart's content
            clone.SetActive(true);
            clone.transform.position = ballNumber4PartsStartPos;
            //5.5 seconds
            Destroy(clone, 5.5f);

            Debug.Log("Destroy old object " + ObjectOld.ToString());
            ObjectNew.transform.position = ballNumber4PartsStartPos;
        }
    }

	void OnCollisionEnter(Collision col)
	{
        if (col.collider.tag == "DestroyTwo") {
            HealthTwo -= 1;
		}
    }

    public static void AddHealthTwo() {
        HealthTwo++;
    }
}