﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameO : MonoBehaviour {
	
	public GameObject ObjectNew;
	public GameObject ObjectOld;
	public static int Health = 1;

    private Vector3 whiteBallPartsStartPos = new Vector3(-5.17f, 41.72f, -4.96f);

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Health <= 0) {
			//ObjectNew.SetActive (true);
			ObjectOld.SetActive (false);

            GameObject clone = Instantiate(ObjectNew, Vector3.one, Quaternion.identity) as GameObject;
            // Modify the clone to your heart's content
            clone.SetActive(true);
            clone.transform.position = whiteBallPartsStartPos;
            //7.5 seconds
            Destroy(clone, 7.5f);

            Debug.Log("Destroy old object " + ObjectOld.ToString());
            ObjectNew.transform.position = whiteBallPartsStartPos;
        }
    }

	void OnCollisionEnter(Collision col)
	{
        if (col.collider.tag == "Destroy") {
            Health -= 1;
		}
    }

    public static void AddHealth() {
        Health++;
    }
}