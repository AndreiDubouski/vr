﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FunButton : MonoBehaviour {

	//2 seconds wait, after button is clicked
	public float gazeTime = 2f;

	//для переключения при просмотре
	private bool state;
	
	private float timer;
	private bool gazeAt;
	
	private Rigidbody rb;
	public GameObject Button;

    public GameObject SurpriseBall;
    private Vector3 RedBallPartsStartPos = new Vector3(-15f, 65f, 30f);

    // Use this for initialization
    void Start ()
	{
		this.rb = GetComponent<Rigidbody>();
		state = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		//FunButtonForFun.transform.position = new Vector3(0f, 0f, 0f);
		
		if(gazeAt == true){
			//Debug.Log ("GazeAt TRUE");
			timer += Time.deltaTime;
			//Debug.Log (timer);

			if(timer >= gazeTime){
				ExecuteEvents.Execute(gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerDownHandler);
				timer = 0f;

				//если один раз нажать надо 
				//GetComponent<Collider> ().enabled = false;
			}
		}
	}

	public void PointerEnter(){
		//Debug.Log ("Pointer Enter");
		gazeAt = true;
	}

	public void PointerExit(){
		//Debug.Log ("Pointer Exit");
		gazeAt = false;
	}

	//Функция, если кнопка нажата
	public void PointerDown(){
		Debug.Log ("FunButton Pointer Down");

        GameObject clone = Instantiate(SurpriseBall, Vector3.one, Quaternion.identity) as GameObject;
        // Modify the clone to your heart's content
        clone.SetActive(true);
        clone.transform.position = RedBallPartsStartPos;
        //6.5 seconds
        Destroy(clone, 6.5f);

    }
}