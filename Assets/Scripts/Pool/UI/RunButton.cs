﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RunButton : MonoBehaviour {

	//2 seconds wait, after button is clicked
	public float gazeTime = 2f;

	private float timer;
	private bool gazeAt;

	//ball, whitch will be moving alll time
	public GameObject whiteBall;
	private Vector3 offsetBall;
    //destroy object, parts white ball
    public GameObject ObjectNew;

    //Step
    private Vector3 playerPos = new Vector3 (0, 0, 0.195f);
    //до белого шара (0,0,41.73)
	private float timerVideo;
	//0.0001 second wait
	public float videoStepTime = 1.0f;
    private int STEPS = 214;
    //private int STEPS = 107;
    private int runStep = 0;
	private bool runMoves = false;

    //STEP TWO
    //ball 4 will move distanse (1.06, 34, 21.73)->(18.2, 34, 38.25) = (17.14, 0, 16.52)
    private int runStepPartTwo = 0;
    private bool runMovesPartTwo = false;
    private int STEPS_PART_TWO = 262;//260
    private Vector3 playerStepPartTwo = new Vector3(0.06592308f, 0, 0.06353846f);
    //ball #4
    public GameObject ballNumber4;

    // Use this for initialization
    void Start () {
		offsetBall = transform.position - whiteBall.transform.position;	
	}
	
	// Update is called once per frame
	void Update () {
		if(gazeAt == true){
			//Debug.Log ("GazeAt TRUE");
			timer += Time.deltaTime;
			//Debug.Log (timer);

			if(timer >= gazeTime){
				ExecuteEvents.Execute(gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerDownHandler);
				timer = 0f;

				//если один раз нажать надо 
				//GetComponent<Collider> ().enabled = false;
			}
		}

        //запуск движения белого шара
        if (runMoves == true) {
            if (runStep < STEPS) {
                //timerVideo += Time.deltaTime;
                //if (timerVideo >= videoStepTime)
                //{
                RunMoveVideo();
                //}
            }
            else{
                runStep = 0;
                runMoves = false;
                runMovesPartTwo = true;
            }
        }
        //parts two
        if (runMovesPartTwo == true)
        {
            if (runStepPartTwo < STEPS_PART_TWO)
            {
                RunMoveVideoPartTwo();
            }
            else
            {
                runStepPartTwo = 0;
                runMoves = false;
                runMovesPartTwo = false;
            }
        }
    }

	public void PointerEnter(){
		//Debug.Log ("Pointer Enter");
		gazeAt = true;
	}

	public void PointerExit(){
		//Debug.Log ("Pointer Exit");
		gazeAt = false;
	}

	//Функция, если кнопка нажата
	public void PointerDown(){
		Debug.Log ("RuntButton Pointer Down");
		// Update is called once per click button
		//whiteBall.transform.position = whiteBall.transform.position + playerPos;

        Debug.Log("run moves");
        runMoves = true;


        //temp
        whiteBall.SetActive(true);
        ObjectNew.SetActive(false);
    }

	private void RunMoveVideo(){
                Debug.Log(message: "Step: " + runStep);
                whiteBall.transform.position = whiteBall.transform.position + playerPos;
                runStep++;
	}

    private void RunMoveVideoPartTwo()
    {
        Debug.Log(message: "Part 2 step: " + runStepPartTwo);
        ballNumber4.transform.position = ballNumber4.transform.position + playerStepPartTwo;
        runStepPartTwo++;
    }
}