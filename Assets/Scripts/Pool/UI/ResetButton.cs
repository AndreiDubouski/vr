﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ResetButton : MonoBehaviour
{

	//2 seconds wait, after button is clicked
	public float gazeTime = 2f;

	private float timer;
	private bool gazeAt;

    //ball, whitch will be moving all time
    public GameObject whiteBall;
    //parts white ball, whitch will be moving all time
    public GameObject ObjectNewWhiteBall;
    
    //ball number 4
    public GameObject BallNumber4;
    public GameObject ObjectNewBallNumber4;

    private Vector3 startBallPosition;
    //координаты частей белого шара
    private Vector3 whiteBallPartsStartPos = new Vector3(1.06f, 34.0f, 21.73f);

    //координаты шара #4
    private Vector3 ballNumber4StartPos = new Vector3(1.06f, 34.0f, 21.73f);
    private Vector3 ballNumber4PartsStartPos = new Vector3(17.04f, 35.27f, 30.27f);

    //ball number 11
    public GameObject BallNumber11;
    //координаты шара #11
    private Vector3 ballNumber11StartPos = new Vector3(18f, 34.0f, 38f);

    // Use this for initialization
    void Start () {
        startBallPosition = new Vector3(0f, 34f, -20f);

    }
	
	// Update is called once per frame
	void Update () {
		if(gazeAt == true){
			//Debug.Log ("GazeAt TRUE");
			timer += Time.deltaTime;
			//Debug.Log (timer);

			if(timer >= gazeTime){
				ExecuteEvents.Execute(gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerDownHandler);
				timer = 0f;

				//если один раз нажать надо 
				//GetComponent<Collider> ().enabled = false;
			}
		}
	}

	public void PointerEnter(){
		//Debug.Log ("Pointer Enter");
		gazeAt = true;
	}

	public void PointerExit(){
		//Debug.Log ("Pointer Exit");
		gazeAt = false;
	}

	//Функция, если кнопка нажата
	public void PointerDown(){
		Debug.Log ("ResetButton Pointer Down");
        whiteBall.transform.position = startBallPosition;
        whiteBall.SetActive(true);

        DestroyGameO.AddHealth();
        ObjectNewWhiteBall.SetActive(false);

        BallNumber4.transform.position = ballNumber4StartPos;
        BallNumber4.SetActive(true);

        DestroyGameObjectTwo.AddHealthTwo();
        ObjectNewBallNumber4.SetActive(false);


        BallNumber11.transform.position = ballNumber11StartPos;
        BallNumber11.SetActive(true);
        //ObjectNew.transform.position = whiteBallPartsStartPos;
    }
}