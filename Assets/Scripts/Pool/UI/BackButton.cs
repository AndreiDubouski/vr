﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BackButton : MonoBehaviour {

	//2 seconds wait, after button is clicked
	public float gazeTime = 2f;

	private float timer;
	private bool gazeAt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(gazeAt == true){
			//Debug.Log ("GazeAt TRUE");
			timer += Time.deltaTime;
			//Debug.Log (timer);

			if(timer >= gazeTime){
				ExecuteEvents.Execute(gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerDownHandler);
				timer = 0f;

				//если один раз нажать надо 
				//GetComponent<Collider> ().enabled = false;
			}
		}
	}

	public void PointerEnter(){
		//Debug.Log ("Pointer Enter");
		gazeAt = true;
	}

	public void PointerExit(){
		//Debug.Log ("Pointer Exit");
		gazeAt = false;
	}

	//Функция, если кнопка нажата
	public void PointerDown(){
		Debug.Log ("BackButton Pointer Down");
	}
}